package micro.service.reportservice.service;

import micro.service.reportservice.dto.StudentDto;
import org.springframework.batch.core.JobExecution;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public interface ImportService {
    JobExecution importStudentFromExcelFile(MultipartFile file) throws IOException, Exception;
    List<StudentDto> readFileExcel(String path) throws IOException;
}
