package micro.service.reportservice.service.impl;

import micro.service.reportservice.dto.StudentDto;
import micro.service.reportservice.service.ImportService;
import micro.service.reportservice.utils.FileUtils;
import org.apache.poi.ss.usermodel.*;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ImportServiceImpl implements ImportService {

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private Job job;

    @Autowired
    private FileUtils fileUtils;
    File tempFile;

    @Override
    public JobExecution importStudentFromExcelFile(MultipartFile file) throws Exception {
        Path tempDir = Files.createTempDirectory("");
        tempFile = tempDir.resolve(Objects.requireNonNull(file.getOriginalFilename())).toFile();
        file.transferTo(tempFile);
        JobParameters jobParameters = new JobParametersBuilder().addString("pathFile", tempFile.getPath()).addDate("date", new Date()).toJobParameters();
        JobExecution jobExecution = jobLauncher.run(job, jobParameters);
        return jobExecution;
    }

    @Override
    public List<StudentDto> readFileExcel(String path) throws IOException {
        Workbook workbook = WorkbookFactory.create(new File(path));
        Sheet sheet = workbook.getSheetAt(0);
        Supplier<Stream<Row>> streamRow = fileUtils.getRowStreamSupplier(sheet);
        List<StudentDto> studentDtos = streamRow.get().skip(10).map(row -> {
            List<String> cellList = fileUtils.getStream(row).map(Cell::getStringCellValue).collect(Collectors.toList());
            StudentDto studentDto = new StudentDto();
            try {
                if (cellList.get(1).matches("[0-9]+")) {
                    studentDto.setStudentCode(cellList.get(1));
                    studentDto.setFirstName(cellList.get(2));
                    studentDto.setLastName(cellList.get(3));
                    studentDto.setBirthDay(new SimpleDateFormat("dd/MM/yy").parse(cellList.get(4)));
                    studentDto.setClassCode(cellList.get(5));
                    return studentDto;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        }).filter(Objects::nonNull).collect(Collectors.toList());
        workbook.close();
        return studentDtos;
    }
}
