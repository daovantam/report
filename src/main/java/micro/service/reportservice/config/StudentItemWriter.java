package micro.service.reportservice.config;

import lombok.extern.slf4j.Slf4j;
import micro.service.reportservice.dto.StudentDto;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.List;

@Slf4j
public class StudentItemWriter implements ItemWriter<StudentDto> {

    @Autowired
    private KafkaTemplate<String, List<? extends StudentDto>> kafkaTemplate;

    @Override
    public void write(List<? extends StudentDto> list) {
        kafkaTemplate.send("topic", list);
    }
}
