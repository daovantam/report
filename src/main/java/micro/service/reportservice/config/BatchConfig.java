package micro.service.reportservice.config;

import micro.service.reportservice.dto.StudentDto;
import micro.service.reportservice.service.ImportService;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.List;

@Configuration
public class BatchConfig {

    private String param;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private ImportService importService;

    @Bean
    @StepScope
    public ItemReader<StudentDto> itemReader(@Value("#{jobParameters['pathFile']}") String path) throws IOException {
        List<StudentDto> studentDtos = importService.readFileExcel(path);
        return new ListItemReader<>(studentDtos);
    }

    @Bean
    @StepScope
    public StudentItemWriter studentItemWriter() {
        return new StudentItemWriter();
    }

    @Bean
    public Step studentStep() throws Exception {
        return stepBuilderFactory.get("studentStep").allowStartIfComplete(true).<StudentDto, StudentDto>chunk(2)
                .reader(itemReader(param)).writer(studentItemWriter()).build();
    }

    @Bean
    public Job studentImportJob() throws Exception {
        return jobBuilderFactory.get("studentImportJob").incrementer(new RunIdIncrementer()).start(studentStep()).build();
    }
}
