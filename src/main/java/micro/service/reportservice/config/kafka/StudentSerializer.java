package micro.service.reportservice.config.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import micro.service.reportservice.dto.StudentDto;
import org.apache.kafka.common.serialization.Serializer;

import java.util.List;
import java.util.Map;

public class StudentSerializer implements Serializer<List<? extends StudentDto>> {
    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

    @Override
    public byte[] serialize(String s, List<? extends StudentDto> studentDtos) {
        byte [] bytes = null;
        ObjectMapper objectMapper = new ObjectMapper();
        if (studentDtos != null) {
            try {
                bytes = objectMapper.writeValueAsString(studentDtos).getBytes();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return bytes;
    }


    @Override
    public void close() {

    }
}
