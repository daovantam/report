package micro.service.reportservice.controller;

import micro.service.reportservice.service.ImportService;
import org.springframework.batch.core.JobExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/import")
public class ImportController {

    @Autowired
    private ImportService importService;

    @PostMapping
    public ResponseEntity<?> importStudent(@RequestParam("file") MultipartFile file) throws Exception {
        JobExecution jobExecution = importService.importStudentFromExcelFile(file);
        return new ResponseEntity<>(jobExecution.getStatus(), HttpStatus.OK);
    }
}
