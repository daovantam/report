package micro.service.reportservice.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class StudentDto {
    private String studentCode;
    private String firstName;
    private String lastName;
    private String classCode;
    private Date birthDay;
}
