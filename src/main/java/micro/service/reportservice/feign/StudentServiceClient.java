package micro.service.reportservice.feign;

import micro.service.reportservice.dto.StudentDto;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@FeignClient(name = "student-service/student", fallbackFactory = ServiceClientFallBackFactory.class)
@RibbonClient(name = "student-service")
public interface StudentServiceClient {
    @PostMapping(value = "/import/excel", produces = "application/json")
    ResponseEntity<String> sendDataToStudentService(List<StudentDto> studentDtos);
}
