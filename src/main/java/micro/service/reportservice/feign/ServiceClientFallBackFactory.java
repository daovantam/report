package micro.service.reportservice.feign;

import feign.hystrix.FallbackFactory;
import micro.service.reportservice.dto.StudentDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
final class ServiceClientFallBackFactory implements FallbackFactory<StudentServiceClient> {

    @Override
    public StudentServiceClient create(Throwable throwable) {
        return studentDtos -> {
            throw new RuntimeException(throwable);
        };
    }
}
